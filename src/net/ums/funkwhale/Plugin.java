package net.ums.funkwhale;

import com.google.common.collect.Iterators;
import net.pms.dlna.DLNAResource;
import net.pms.external.AdditionalFoldersAtRoot;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.swing.*;
import java.util.Iterator;

public class Plugin implements AdditionalFoldersAtRoot {
    private static final Logger LOGGER = LoggerFactory.getLogger(Plugin.class);

    public static final String NAME = "Funkwhale";

    @Override
    public JComponent config() {
        return null;
    }

    @Override
    public String name() {
        return NAME;
    }

    @Override
    public void shutdown() {

    }

    @Override
    public Iterator<DLNAResource> getChildren() {
        try {
            return FunkwhaleConfiguration.getFunkwhaleResources().stream().map(r -> (DLNAResource)r).iterator();
        } catch (Exception e) {
            LOGGER.warn("Could not open Funkwhale Config");
            LOGGER.debug("",e);
        }
        return Iterators.emptyIterator();
    }
}
