package net.ums.funkwhale.dlna;

import com.google.gson.JsonObject;
import net.pms.dlna.DLNAResource;
import net.ums.funkwhale.Credentials;
import net.ums.funkwhale.api.data.Playlist;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static net.ums.funkwhale.FunkwhaleUtils.*;
import static net.ums.funkwhale.dlna.Root.API_PATH;

public class Playlists extends Resource {
    private static final Logger LOGGER = LoggerFactory.getLogger(Playlists.class);
    private static final int MAX_PLAYLIST_SIZE = 7000;

    public Playlists(String name, String path, Credentials credentials, String thumbnailIcon) {
        super(name, path, credentials, thumbnailIcon);
    }

    @Override
    protected List<DLNAResource> getDLNAResources(JsonObject response) {
        return Arrays.stream(gson.fromJson(response.getAsJsonArray("results"), Playlist[].class))
                .map(this::convertPlaylistTrack)
                .collect(Collectors.toList());
    }

    @Override
    protected DLNAResource getNext(String path) {
        return new Playlists("Next",path,getCredentials(),thumbnailIcon);
    }

    private Tracks convertPlaylistTrack(Playlist playlist){
        return new PlaylistTracks(playlist.getName(),getPath(playlist.getId()),getCredentials(),null);
    }

    private String getPath(long id){
        return API_PATH + "playlists/" + id + "/tracks?" + getParameterString(getParameters());
    }

    @Override
    protected Map<String, String> getParameters() {
        HashMap<String, String> map = new HashMap<>(super.getParameters());
        map.put("page_size",MAX_PLAYLIST_SIZE + "");
        return map;

    }
}
