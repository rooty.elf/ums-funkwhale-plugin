package net.ums.funkwhale.dlna;

import com.google.gson.JsonObject;
import net.pms.dlna.DLNAResource;
import net.ums.funkwhale.Credentials;
import net.ums.funkwhale.api.data.PlaylistTrack;
import net.ums.funkwhale.api.data.Track;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class PlaylistTracks extends Tracks{
    public PlaylistTracks(String name, String path, Credentials credentials, String thumbnailIcon) {
        super(name, path, credentials, thumbnailIcon);
    }
    @Override
    protected List<DLNAResource> getDLNAResources(JsonObject response){
        return Arrays.stream(gson.fromJson(response.getAsJsonArray("results"), PlaylistTrack[].class))
                .map(PlaylistTrack::getTrack)
                .map(this::processResultElement)
                .collect(Collectors.toList());
    }

    @Override
    protected DLNAResource getNext(String path) {
        return new PlaylistTracks("Next",path,getCredentials(),thumbnailIcon);
    }

}
