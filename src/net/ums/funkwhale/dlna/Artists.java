package net.ums.funkwhale.dlna;

import com.google.gson.JsonObject;
import net.pms.dlna.DLNAResource;
import net.pms.dlna.virtual.VirtualFolder;
import net.ums.funkwhale.Credentials;
import net.ums.funkwhale.api.data.Album;
import net.ums.funkwhale.api.data.Artist;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static net.ums.funkwhale.dlna.Root.API_PATH;
import static net.ums.funkwhale.FunkwhaleUtils.*;

public class Artists extends Resource {
    public Artists(String name, String path, Credentials credentials, String thumbnailIcon) {
        super(name, path, credentials, thumbnailIcon);
    }

    @Override
    protected List<DLNAResource> getDLNAResources(JsonObject response) {
        Artist[] artists = gson.fromJson(response.getAsJsonArray("results"), Artist[].class);
        return Arrays.stream(artists).map(this::getArtistFolder).collect(Collectors.toList());
    }

    @Override
    protected DLNAResource getNext(String path) {
        return new Artists("Next",path,getCredentials(),thumbnailIcon);
    }

    private VirtualFolder getArtistFolder(Artist artist){
        VirtualFolder virtualFolder = new VirtualFolder(artist.getName(),null);
        Arrays.stream(artist.getAlbums()).map(this::getAlbum).forEach(virtualFolder::addChild);
        return virtualFolder;
    }

    private Tracks getAlbum(Album album){
        return new Tracks(album.getTitle(),getAlbumPath(album.getId()),getCredentials(),null);
    }


    private String getAlbumPath(long id){
        return API_PATH + "/tracks/?" + getParameterString(getParameters()) + "&album=" + id;
    }
}
