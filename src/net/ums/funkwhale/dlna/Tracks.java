package net.ums.funkwhale.dlna;

import com.google.gson.*;
import net.pms.configuration.RendererConfiguration;
import net.pms.dlna.DLNAMediaAudio;
import net.pms.dlna.DLNAMediaInfo;
import net.pms.dlna.DLNAResource;
import net.pms.formats.Format;
import net.pms.formats.FormatFactory;
import net.ums.funkwhale.Credentials;
import net.ums.funkwhale.api.data.Album;
import net.ums.funkwhale.api.data.Artist;
import net.ums.funkwhale.api.data.Track;
import net.ums.funkwhale.api.data.Upload;
import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.LocalDate;
import java.util.*;
import java.util.stream.Collectors;

import static net.ums.funkwhale.FunkwhaleUtils.*;

public class Tracks extends Resource {
    private static final Logger LOGGER = LoggerFactory.getLogger(Tracks.class);

    public Tracks(String name, String path, Credentials credentials, String thumbnailIcon) {
        super(name, path, credentials, thumbnailIcon);
    }

    @Override
    protected List<DLNAResource> getDLNAResources(JsonObject response){
        return Arrays.stream(gson.fromJson(response.getAsJsonArray("results"), Track[].class))
                .map(this::processResultElement)
                .collect(Collectors.toList());
    }

    @Override
    protected DLNAResource getNext(String path) {
        return new Tracks("Next",path,getCredentials(),thumbnailIcon);
    }

    protected DLNAResource processResultElement(Track track){
        Upload upload = getBestUpload(track.getUploads(), getDefaultRenderer());
        Format format = FormatFactory.getAssociatedFormat('.' + upload.getExtension());
        Stream resource = new Stream(track.getTitle(), getServer() + upload.getListenUrl(), getDefaultCoverUrlForAlbum(track.getAlbum()), format.getType());
        resource.setMedia(buildMedia(track,upload));
        resource.setFormat(FormatFactory.getAssociatedFormat('.' + upload.getExtension()));
        return resource;
    }

    private DLNAMediaInfo buildMedia(Track track, Upload upload){
        DLNAMediaInfo media = new DLNAMediaInfo();
        media.setDuration((double)upload.getDuration());
        media.setBitrate((int) (upload.getBitrate() / 8));
        media.setSize(upload.getSize());
        media.setMimeType(upload.getMimetype());
        media.setMediaparsed(true);
        DLNAMediaAudio audio = buildAudioMedia(track, upload);
        media.setAudioTracks(Collections.singletonList(audio));
        media.setYear(audio.getYear() + "");

        return media;
    }

    private DLNAMediaAudio buildAudioMedia(Track track, Upload upload){
        Album album = track.getAlbum();
        Artist artist = track.getArtist();
        Artist albumArtist = album.getArtist();

        DLNAMediaAudio audio = new DLNAMediaAudio();
        audio.setAlbum(album.getTitle());
        audio.setArtist(artist.getName());
        audio.setAlbumArtist(albumArtist.getName());
        audio.setBitRate((int) upload.getBitrate());
        audio.setCodecA(upload.getMimetype().split("/")[1]);
        audio.setSongname(track.getTitle());
        audio.setTrack(track.getPosition());

        if(CollectionUtils.isNotEmpty(Arrays.asList(track.getTags()))){
            audio.setGenre(track.getTags()[0]);
        }
        LocalDate date = album.getReleaseDate();
        audio.setYear(date.getYear());
        return audio;
    }

    private Upload getBestUpload(Upload[] uploads, RendererConfiguration renderer){
        return Arrays.stream(uploads).sorted(Comparator.comparingLong(Upload::getBitrate))
                .filter(u -> renderer.supportsFormat(Objects.requireNonNull(FormatFactory.getAssociatedFormat('.' + u.getExtension())))).findFirst()
                .orElseGet(() -> uploads[0]);
    }
}
