package net.ums.funkwhale.dlna;

import com.google.gson.JsonObject;
import net.pms.dlna.DLNAResource;
import net.ums.funkwhale.Credentials;
import net.ums.funkwhale.api.data.Album;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static net.ums.funkwhale.dlna.Root.API_PATH;
import static net.ums.funkwhale.FunkwhaleUtils.*;

public class Albums extends Resource {
    public Albums(String name, String path, Credentials credentials, String thumbnailIcon) {
        super(name, path, credentials, thumbnailIcon);
    }

    @Override
    protected List<DLNAResource> getDLNAResources(JsonObject response) {
        return Arrays.stream(gson.fromJson(response.getAsJsonArray("results"), Album[].class))
                .map(this::getTracksInAlbum)
                .collect(Collectors.toList());
    }

    @Override
    protected DLNAResource getNext(String path) {
        return new Albums("Next",path,getCredentials(),thumbnailIcon);
    }

    private Tracks getTracksInAlbum(Album album){
        return new Tracks(album.getTitle(),getPath(album.getId()),getCredentials(),getDefaultCoverUrlForAlbum(album));
    }

    private String getPath(long id){
        return API_PATH + "/tracks/?" + getParameterString(getParameters()) + "&album=" + id;
    }
}
