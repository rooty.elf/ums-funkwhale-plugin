package net.ums.funkwhale.dlna;

import net.pms.dlna.WebStream;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.net.URL;

public class Stream extends WebStream {
    private static final Logger LOGGER = LoggerFactory.getLogger(Stream.class);

    public Stream(String fluxName, String url, String thumbURL, int type) {
        super(fluxName, url, thumbURL, type);
    }

    @Override
    public InputStream getInputStream() {
        try {
            return new URL(getUrl()).openStream();
        } catch (IOException e) {
            LOGGER.error("Could not open stream for Funkwhale Resource " + getUrl());
            LOGGER.debug("",e);
        }
        return null;
    }

    @Override
    public long length() {
        if (getMedia() != null && getMedia().isMediaparsed()) {
            return getMedia().getSize();
        }
        LOGGER.warn("Could not determine size for Funkwhale Stream.");
        return super.length();
    }
}
