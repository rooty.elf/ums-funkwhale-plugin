package net.ums.funkwhale.dlna;

import net.pms.dlna.virtual.VirtualFolder;
import net.ums.funkwhale.Credentials;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collections;
import java.util.Map;

import static net.ums.funkwhale.FunkwhaleUtils.*;


public class Root extends VirtualFolder {
    private static final Logger LOGGER = LoggerFactory.getLogger(Root.class);

    protected static final String API_PATH = "/api/v1/";

    private final String server;
    private final Credentials credentials;
    private final Map<String, String> parameters;

    public Root(String server, Credentials credentials, Map<String,String> parameters){
        super(getNameForFunkwhaleResource(server,credentials), server + "/front/favicon.png");
        this.server = server;
        this.credentials = credentials;
        if(parameters != null) {
            this.parameters = parameters;
        }
        else {
            this.parameters = Collections.emptyMap();
        }
        initializeChildren();
    }

    private void initializeChildren(){
        addChild(new Artists("Artists",API_PATH + "artists?has_albums=true&" + getParameterString(getParameters()),credentials,null));
        addChild(new Albums("Albums",API_PATH + "albums?" + getParameterString(getParameters()),credentials,null));
        addChild(new Tracks("Tracks",API_PATH + "tracks?" + getParameterString(getParameters()),credentials,null));
        addChild(new Playlists("Playlists",API_PATH + "playlists?" + getParameterString(getParameters()),credentials,null));
    }

    public String getServer() {
        return server;
    }

    public Credentials getCredentials() {
        return credentials;
    }

    public Map<String, String> getParameters() {
        return parameters;
    }
}