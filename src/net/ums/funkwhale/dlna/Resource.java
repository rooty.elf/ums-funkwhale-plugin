package net.ums.funkwhale.dlna;

import com.google.gson.*;
import net.pms.dlna.DLNAResource;
import net.pms.dlna.virtual.VirtualFolder;
import net.ums.funkwhale.Credentials;
import net.ums.funkwhale.api.GsonFactory;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.IntStream;

import static net.ums.funkwhale.FunkwhaleUtils.*;

public abstract class Resource extends VirtualFolder {
    protected final Gson gson = GsonFactory.getGson();

    private static final Logger LOGGER = LoggerFactory.getLogger(Resource.class);

    private final String path;
    private final Credentials credentials;

    public Resource(String name, String path, Credentials credentials, String thumbnailIcon) {
        super(name, thumbnailIcon);
        this.path = path;
        this.credentials = credentials;
        setDiscovered(true);
    }

    @Override
    public void doRefreshChildren() {
        try (CloseableHttpResponse response = HttpClients.createDefault().execute(new HttpGet(getServer() + path))){
            JsonObject result = (JsonObject) JsonParser.parseString(EntityUtils.toString(response.getEntity()));
            List<DLNAResource> newChildren = getDLNAResources(result);
            JsonElement next = result.get("next");
            if(next instanceof JsonPrimitive){
                newChildren.add(getNext(getPathWithoutServer(next.getAsString())));
            }
            if(hasChanged(newChildren)){
                getChildren().clear();
                newChildren.forEach(this::addChild);
            }
        } catch (Exception e) {
            LOGGER.error("Error while connection to Funkwhale server " + getServer());
            LOGGER.debug("",e);
        }
    }

    private boolean hasChanged(List<DLNAResource> newChildren){
        List<DLNAResource> oldChildren = getChildren();
        if(newChildren.size() != oldChildren.size()){
            return true;
        }

        return !IntStream.range(0, newChildren.size())
                .allMatch(i -> StringUtils.equals(oldChildren.get(i).write(), newChildren.get(i).write()));
    }

    @Override
    public String write() {
        return name + ">" + path + ">" + thumbnailIcon + ">" + getSpecificType();
    }

    protected abstract List<DLNAResource> getDLNAResources(JsonObject response);

    protected abstract DLNAResource getNext(String path);

    protected String getServer()
    {
        DLNAResource parent = getParent();
        while (parent != null){
            if(parent instanceof Root){
                return ((Root)parent).getServer();
            }
            parent = parent.getParent();
        }

        throw new IllegalStateException(Tracks.class.getName() + " needs " + Root.class.getName() + " as parent.");
    }

    protected Map<String,String> getParameters(){
        Map<String,String> parameters = null;
        DLNAResource parent = getParent();
        while (parent != null){
            if(parent instanceof Root){
                parameters = ((Root)parent).getParameters();
                break;
            }
            parent = parent.getParent();
        }
        if(parameters != null){
            return parameters;
        }
        LOGGER.warn("No parameters map found for Funkwhale Resource " + getName());
        return Collections.emptyMap();
    }

    @Override
    public boolean isRefreshNeeded() {
        return true;
    }

    protected String getPath() {
        return path;
    }

    public Credentials getCredentials() {
        return credentials;
    }
}
