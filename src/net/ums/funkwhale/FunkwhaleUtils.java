package net.ums.funkwhale;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import net.ums.funkwhale.api.data.Album;
import net.ums.funkwhale.api.data.Cover;

import java.util.Map;
import java.util.stream.Collectors;

public class FunkwhaleUtils {
    public static String getNameForFunkwhaleResource(String server, Credentials credentials){
        boolean local = server.contains("127.0.0.1") || server.contains("localhost");
        String name;
        if(local){
            name = Plugin.NAME;
        }
        else{
            name = server.split("//",2)[1];
            if(name.endsWith("/"))
            {
                name = name.substring(0,name.length() - 1);
            }
        }
        if(credentials != null){
            name = getUsernameForCredentials(credentials) + "@" + name;
        }
        if(!local){
            name += " (" + Plugin.NAME + ")";
        }
        return name ;
    }

    public static String getDefaultCoverUrlForAlbum(Album album)
    {
        if(album != null){
            Cover cover = album.getCover();
            if(cover !=null){
                return cover.getUrls().getLargeSquareCrop();
            }
        }
        return null;
    }

    public static String getPathWithoutServer(String path){
        return path.substring(path.indexOf("/",path.indexOf("//") + 2));
    }

    public static String getParameterString(Map<String,String> parameters){
        return parameters.entrySet().stream().map(e -> e.getKey() + "=" + e.getValue()).collect(Collectors.joining("&"));
    }

    public static String getUsernameForCredentials(Credentials credentials){
        return "testuser";
    }
}
