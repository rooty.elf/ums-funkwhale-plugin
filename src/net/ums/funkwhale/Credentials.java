package net.ums.funkwhale;

public class Credentials {
    private final String clientId;
    private final String clientSecret;
    private final String accessToken;

    public Credentials(String clientId, String clientSecret, String accessToken) {
        this.clientId = clientId;
        this.clientSecret = clientSecret;
        this.accessToken = accessToken;
    }

    public String getClientId() {
        return clientId;
    }

    public String getClientSecret() {
        return clientSecret;
    }

    public String getAccessToken() {
        return accessToken;
    }
}
