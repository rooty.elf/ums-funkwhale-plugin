package net.ums.funkwhale.api.adapter;

import com.google.gson.*;
import com.google.gson.internal.bind.JsonTreeReader;
import com.google.gson.internal.bind.ReflectiveTypeAdapterFactory;
import com.google.gson.reflect.TypeToken;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;
import com.google.gson.stream.JsonWriter;

import java.io.IOException;
import java.time.LocalDate;

public class FunkwhaleTypeAdapterFactory implements TypeAdapterFactory {

    @Override
    public <T> TypeAdapter<T> create(Gson gson, TypeToken<T> type) {
        TypeAdapter<T> delegateAdapter = gson.getDelegateAdapter(this, type);

        return new TypeAdapter<T>() {
            @Override
            public void write(JsonWriter out, T value) throws IOException {
                delegateAdapter.write(out,value);
            }

            @Override
            public T read(JsonReader in) throws IOException {
                if(in.peek() == JsonToken.NUMBER && delegateAdapter instanceof ReflectiveTypeAdapterFactory.Adapter){
                    JsonObject object = new JsonObject();
                    object.add("id", new JsonPrimitive(in.nextLong()));
                    in = new JsonTreeReader(object);
                }
                else if(in.peek() == JsonToken.STRING && type.getRawType().equals(LocalDate.class)){
                    return (T) LocalDate.parse(in.nextString());
                }
                return delegateAdapter.read(in);
            }
        };
    }
}
