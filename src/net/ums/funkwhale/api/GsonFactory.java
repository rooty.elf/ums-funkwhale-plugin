package net.ums.funkwhale.api;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import net.ums.funkwhale.api.adapter.FunkwhaleTypeAdapterFactory;

public class GsonFactory {
    private static final Gson GSON = new GsonBuilder()
            .registerTypeAdapterFactory(new FunkwhaleTypeAdapterFactory())
            .create();

    public static Gson getGson(){
        return GSON;
    }
}
