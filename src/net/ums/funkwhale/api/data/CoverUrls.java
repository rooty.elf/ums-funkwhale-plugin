package net.ums.funkwhale.api.data;

import com.google.gson.annotations.SerializedName;

public class CoverUrls {
    private String source;
    private String original;
    @SerializedName(value = "medium_square_crop")
    private String mediumSquareCrop;
    @SerializedName(value = "large_square_crop")
    private String largeSquareCrop;

    public String getSource() {
        return source;
    }

    public String getOriginal() {
        return original;
    }

    public String getMediumSquareCrop() {
        return mediumSquareCrop;
    }

    public String getLargeSquareCrop() {
        return largeSquareCrop;
    }
}
