package net.ums.funkwhale.api.data;

import com.google.gson.annotations.SerializedName;

import java.util.Date;

public class PlaylistTrack {
    private int index;
    private Track track;
    @SerializedName(value = "creation_date")
    private Date creationDate;

    public int getIndex() {
        return index;
    }

    public Track getTrack() {
        return track;
    }

    public Date getCreationDate() {
        return creationDate;
    }
}
