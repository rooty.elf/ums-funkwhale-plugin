package net.ums.funkwhale.api.data;

import com.google.gson.annotations.SerializedName;

import java.util.Date;

public class Cover {
    private String uuid;
    private long size;
    private String mimetype;
    @SerializedName(value = "creation_date")
    private Date creationDate;
    private CoverUrls urls;

    public String getUuid() {
        return uuid;
    }

    public long getSize() {
        return size;
    }

    public String getMimetype() {
        return mimetype;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public CoverUrls getUrls() {
        return urls;
    }
}
