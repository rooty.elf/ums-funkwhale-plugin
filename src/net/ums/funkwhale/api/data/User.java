package net.ums.funkwhale.api.data;

import com.google.gson.annotations.SerializedName;

import java.util.Date;

public class User {
    private String fid;
    private String url;
    private String name;
    @SerializedName(value = "preferred_username")
    private String preferredUsername;
    @SerializedName(value = "full_username")
    private String fullUsername;
    private String summary;
    private String domain;
    @SerializedName(value = "creation_date")
    private Date creationDate;
    @SerializedName(value = "last_fetch_date")
    private Date lastFetchDate;
    private String type;
    @SerializedName(value = "manually_approves_followers")
    private boolean manuallyApprovesFollowers;
    @SerializedName(value = "is_local")
    private boolean local;
}
