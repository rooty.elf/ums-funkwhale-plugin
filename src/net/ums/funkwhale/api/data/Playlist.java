package net.ums.funkwhale.api.data;

import com.google.gson.annotations.SerializedName;

import java.util.Date;

public class Playlist {
    private long id;
    private String name;
    @SerializedName(value = "privacy_level")
    private String privacyLevel;
    @SerializedName(value = "tracks_count")
    private int tracksCount;
    @SerializedName(value = "album_covers")
    private String[] albumCovers;
    private Long duration;
    @SerializedName(value = "is_playable")
    private boolean playable;
    private User actor;
    @SerializedName(value = "modification_date")
    private Date modificationDate;
    @SerializedName(value = "creation_date")
    private Date creationDate;

    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getPrivacyLevel() {
        return privacyLevel;
    }

    public int getTracksCount() {
        return tracksCount;
    }

    public String[] getAlbumCovers() {
        return albumCovers;
    }

    public Long getDuration() {
        return duration;
    }

    public boolean isPlayable() {
        return playable;
    }

    public User getActor() {
        return actor;
    }

    public Date getModificationDate() {
        return modificationDate;
    }

    public Date getCreationDate() {
        return creationDate;
    }
}
