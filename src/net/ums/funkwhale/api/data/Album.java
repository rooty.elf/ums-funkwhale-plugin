package net.ums.funkwhale.api.data;

import com.google.gson.annotations.SerializedName;

import java.time.LocalDate;
import java.util.Date;

public class Album {
    private long id;
    private String fid;
    private String mbid;
    private String title;
    private Artist artist;
    private Cover cover;
    @SerializedName(value = "tracks_count")
    private int tracksCount;
    @SerializedName(value = "is_playable")
    private boolean playable;
    @SerializedName(value = "release_date")
    private LocalDate releaseDate;
    @SerializedName(value = "creation_date")
    private Date creationDate;
    @SerializedName(value = "is_local")
    private boolean local;

    public Album(long id) {
        this.id = id;
    }

    public long getId() {
        return id;
    }

    public String getFid() {
        return fid;
    }

    public String getMbid() {
        return mbid;
    }

    public String getTitle() {
        return title;
    }

    public Artist getArtist() {
        return artist;
    }

    public Cover getCover() {
        return cover;
    }

    public int getTracksCount() {
        return tracksCount;
    }

    public boolean isPlayable() {
        return playable;
    }

    public LocalDate getReleaseDate() {
        return releaseDate;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public boolean isLocal() {
        return local;
    }
}
