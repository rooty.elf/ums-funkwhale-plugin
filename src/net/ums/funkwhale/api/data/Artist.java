package net.ums.funkwhale.api.data;

import com.google.gson.annotations.SerializedName;

import java.util.Date;

public class Artist {
    private long id;
    private String fid;
    private String mbid;
    private String name;
    private Cover cover;
    private Album[] albums;
    @SerializedName(value = "tracks_count")
    private int tracksCount;
    private String[] tags;
    @SerializedName(value = "attributed_to")
    private User attributedTo;
    private String channel;
    @SerializedName(value = "content_category")
    private String contentCategory;
    @SerializedName(value = "creation_date")
    private Date creationDate;
    @SerializedName(value = "modification_date")
    private Date modificationDate;
    @SerializedName(value = "is_local")
    private boolean local;

    public Artist(long id) {
        this.id = id;
    }

    public long getId() {
        return id;
    }

    public String getFid() {
        return fid;
    }

    public String getMbid() {
        return mbid;
    }

    public String getName() {
        return name;
    }

    public Cover getCover() {
        return cover;
    }

    public Album[] getAlbums() {
        return albums;
    }

    public int getTracksCount() {
        return tracksCount;
    }

    public String[] getTags() {
        return tags;
    }

    public User getAttributedTo() {
        return attributedTo;
    }

    public String getChannel() {
        return channel;
    }

    public String getContentCategory() {
        return contentCategory;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public Date getModificationDate() {
        return modificationDate;
    }

    public boolean isLocal() {
        return local;
    }
}
