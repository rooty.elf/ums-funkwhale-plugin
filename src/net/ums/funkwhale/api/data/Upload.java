package net.ums.funkwhale.api.data;

import com.google.gson.annotations.SerializedName;

public class Upload {
    private String uuid;
    @SerializedName(value = "listen_url")
    private String listenUrl;
    private long size;
    private long duration;
    private long bitrate;
    private String mimetype;
    private String extension;
    @SerializedName(value = "is_local")
    private boolean local;

    public String getUuid() {
        return uuid;
    }

    public String getListenUrl() {
        return listenUrl;
    }

    public long getSize() {
        return size;
    }

    public long getDuration() {
        return duration;
    }

    public long getBitrate() {
        return bitrate;
    }

    public String getMimetype() {
        return mimetype;
    }

    public String getExtension() {
        return extension;
    }

    public boolean isLocal() {
        return local;
    }
}
