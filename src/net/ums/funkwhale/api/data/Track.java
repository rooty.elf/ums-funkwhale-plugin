package net.ums.funkwhale.api.data;

import com.google.gson.annotations.SerializedName;

import java.util.Date;

public class Track {
    private long id;
    private String fid;
    private String mbid;
    private Cover cover;
    private String title;
    private Album album;
    private Artist artist;
    private int position;
    @SerializedName(value = "disc_number")
    private int discNumber;
    private String[] tags;
    private Upload[] uploads;
    @SerializedName(value = "listen_url")
    private String listenUrl;
    @SerializedName(value = "downloads_count")
    private int downloadsCount;
    private String copyright;
    private String license;
    @SerializedName(value = "attributed_to")
    private User attributedTo;
    @SerializedName(value = "creation_date")
    private Date creationDate;
    @SerializedName(value = "is_local")
    private boolean local;

    public Track(long id) {
        this.id = id;
    }

    public long getId() {
        return id;
    }

    public String getFid() {
        return fid;
    }

    public String getMbid() {
        return mbid;
    }

    public Cover getCover() {
        return cover;
    }

    public String getTitle() {
        return title;
    }

    public Album getAlbum() {
        return album;
    }

    public Artist getArtist() {
        return artist;
    }

    public int getPosition() {
        return position;
    }

    public int getDiscNumber() {
        return discNumber;
    }

    public String[] getTags() {
        return tags;
    }

    public Upload[] getUploads() {
        return uploads;
    }

    public String getListenUrl() {
        return listenUrl;
    }

    public int getDownloadsCount() {
        return downloadsCount;
    }

    public String getCopyright() {
        return copyright;
    }

    public String getLicense() {
        return license;
    }

    public User getAttributedTo() {
        return attributedTo;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public boolean isLocal() {
        return local;
    }
}
