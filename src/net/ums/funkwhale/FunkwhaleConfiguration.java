package net.ums.funkwhale;

import com.google.gson.*;
import com.google.gson.reflect.TypeToken;
import com.sun.jna.Platform;
import net.pms.configuration.Build;
import net.pms.util.FileUtil;
import net.pms.util.PropertiesUtil;
import net.ums.funkwhale.dlna.Root;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

public class FunkwhaleConfiguration {
    //Copy Pasted from PmsConfiguration.java
    protected static final String PROFILE_DIRECTORY_NAME = Build.getProfileDirectoryName();
    protected static final String DEFAULT_PROFILE_FILENAME = "Funkwhale.json";
    protected static final String ENV_PROFILE_PATH = "UMS_PROFILE";
    protected static final String DEFAULT_WEB_CONF_FILENAME = "WEB.conf";
    protected static final String DEFAULT_CREDENTIALS_FILENAME = "UMS.cred";

    // Path to directory containing UMS config files
    protected static final String PROFILE_DIRECTORY;

    // Absolute path to profile file e.g. /path/to/UMS.conf
    protected static final String PROFILE_PATH;

    // Absolute path to WEB.conf file e.g. /path/to/WEB.conf
    protected static String WEB_CONF_PATH;

    // Absolute path to skel (default) profile file e.g. /etc/skel/.config/universalmediaserver/UMS.conf
    // "project.skelprofile.dir" project property
    protected static final String SKEL_PROFILE_PATH;

    protected static final String PROPERTY_PROFILE_PATH = "ums.profile.path";
    protected static final String SYSTEM_PROFILE_DIRECTORY;
    static {
        // first of all, set up the path to the default system profile directory
        if (Platform.isWindows()) {
            String programData = System.getenv("ALLUSERSPROFILE");

            if (programData != null) {
                SYSTEM_PROFILE_DIRECTORY = String.format("%s\\%s", programData, PROFILE_DIRECTORY_NAME);
            } else {
                SYSTEM_PROFILE_DIRECTORY = ""; // i.e. current (working) directory
            }
        } else if (Platform.isMac()) {
            SYSTEM_PROFILE_DIRECTORY = String.format(
                    "%s/%s/%s",
                    System.getProperty("user.home"),
                    "/Library/Application Support",
                    PROFILE_DIRECTORY_NAME
            );
        } else {
            String xdgConfigHome = System.getenv("XDG_CONFIG_HOME");

            if (xdgConfigHome == null) {
                SYSTEM_PROFILE_DIRECTORY = String.format("%s/.config/%s", System.getProperty("user.home"), PROFILE_DIRECTORY_NAME);
            } else {
                SYSTEM_PROFILE_DIRECTORY = String.format("%s/%s", xdgConfigHome, PROFILE_DIRECTORY_NAME);
            }
        }

        // now set the profile path. first: check for a custom setting.
        // try the system property, typically set via the profile chooser
        String customProfilePath = System.getProperty(PROPERTY_PROFILE_PATH);

        // failing that, try the environment variable
        if (StringUtils.isBlank(customProfilePath)) {
            customProfilePath = System.getenv(ENV_PROFILE_PATH);
        }

        // if customProfilePath is still blank, the default profile dir/filename is used
        FileUtil.FileLocation profileLocation = FileUtil.getFileLocation(
                customProfilePath,
                SYSTEM_PROFILE_DIRECTORY,
                DEFAULT_PROFILE_FILENAME
        );
        PROFILE_PATH = profileLocation.getFilePath();
        PROFILE_DIRECTORY = profileLocation.getDirectoryPath();

        // Set SKEL_PROFILE_PATH for Linux systems
        String skelDir = PropertiesUtil.getProjectProperties().get("project.skelprofile.dir");
        if (Platform.isLinux() && StringUtils.isNotBlank(skelDir)) {
            SKEL_PROFILE_PATH = FilenameUtils.normalize(
                    new File(
                            new File(
                                    skelDir,
                                    PROFILE_DIRECTORY_NAME
                            ).getAbsolutePath(),
                            DEFAULT_PROFILE_FILENAME
                    ).getAbsolutePath()
            );
        } else {
            SKEL_PROFILE_PATH = null;
        }
    }

    public static List<Root> getFunkwhaleResources() throws FileNotFoundException {
        FileUtil.FileLocation profileLocation = FileUtil.getFileLocation(
                System.getProperty("ums.profile.path"),
                SYSTEM_PROFILE_DIRECTORY,
                DEFAULT_PROFILE_FILENAME
        );

        JsonArray configs = (JsonArray) JsonParser.parseReader(new FileReader(profileLocation.getFilePath()));

        return StreamSupport.stream(configs.spliterator(),false).map(e -> getFunkwhaleResource((JsonObject) e)).collect(Collectors.toList());
    }

    private static Root getFunkwhaleResource(JsonObject config){
        Gson gson = new Gson();
        Credentials credentials = null;
        JsonElement credentialsJson = config.get("credentials");
        if(credentialsJson != null){
            credentials = gson.fromJson(credentialsJson, Credentials.class);
        }
        Map<String,String> parameters = null;
        JsonElement parametersJson = config.get("parameters");
        if(parametersJson != null){
            parameters = gson.fromJson(parametersJson,new TypeToken<Map<String,String>>(){}.getType());
        }
        return new Root(config.get("server").getAsString(),credentials,parameters);
    }
}
